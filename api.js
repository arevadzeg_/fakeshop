import { SERVER_ADDRESS } from "./config.js";

async function getData(url) {
  const response = await fetch(url);
  const responseJson = await response.json();
  return responseJson;
}

export async function getProducts() {
  return await getData(SERVER_ADDRESS + "products");
}

export async function getCategories() {
  return await getData(SERVER_ADDRESS + "products/categories");
}
