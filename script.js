import { getProducts, getCategories } from "./api.js";
import { generateHTML } from "./products.js";

const catalog = document.querySelector(".catalog");
const searchButton = document.querySelector("#search-btn");
const sortSelection = document.querySelector(".search-sort__select");
const loadGif = document.querySelector("#load-gif");
const categories = document.querySelector(".filters__category select");

const gifLoadingState = (state) => {
  if (state) {
    loadGif.style.display = "block";
    catalog.style.display = "none";
  } else {
    loadGif.style.display = "none";
    catalog.style.display = "flex";
  }
};

getProducts().then((items) => {
  catalog.innerHTML = generateHTML(items);
  gifLoadingState(false);
});

//**********************SEARCH*********

searchButton.addEventListener("click", () => {
  gifLoadingState(true);
  const query = document.querySelector("#search-text").value.toLowerCase();
  getProducts().then((items) => {
    catalog.classList.remove("catalog-no-result");
    const filteredItems = items.filter(
      (item) => item.title.toLowerCase().indexOf(query) != -1
    );
    if (filteredItems.length == 0) {
      catalog.innerHTML = "";
      catalog.classList.add("catalog-no-result");
    } else {
      catalog.innerHTML = generateHTML(filteredItems);
    }
    gifLoadingState(false);
  });
});

// ***************SORT*****************

function actualSort() {
  let items = document.querySelectorAll(".catalog__item-wraper");
  const itemsArrey = Array.from(items);
  const how = document.querySelector(".search-sort__select").value;
  if (how == "asc") {
    itemsArrey.sort(
      (a, b) =>
        parseFloat(a.children[4].innerText) -
        parseFloat(b.children[4].innerText)
    );
  } else if (how == "desc") {
    itemsArrey.sort(
      (a, b) =>
        parseFloat(b.children[4].innerText) -
        parseFloat(a.children[4].innerText)
    );
  }
  let html = "";
  for (let item of itemsArrey) {
    html += item.outerHTML;
  }
  catalog.innerHTML = html;
}

sortSelection.addEventListener("change", () => {
  gifLoadingState(true);
  setTimeout(() => {
    actualSort();
    gifLoadingState(false);
  }, 400);
});

// *****************LISTS CATEGORIES************************

getCategories().then((items) => {
  let html = " <option disabled selected hidden>Select Niche</option>";
  for (let item of items) {
    html += `<option>${item} </option>`;
  }
  categories.innerHTML += html;
});
