export function generateProducts(item) {
  return `<div class='catalog__item-wraper'>
  <input type="checkbox" class="catalog__checkbox" />
  <p class="catalog__add">Add to Inventory</p>

    <img class='catalog__img' src='${item.image}'/>
                <p class='catalog__title'>${item.title}</p>
                <p class='catalog__price'>${item.price}$</p>
                </div>
              `;
}

export function generateHTML(items) {
  let html = "";
  for (let item of items) {
    html += generateProducts(item);
  }
  return html;
}
